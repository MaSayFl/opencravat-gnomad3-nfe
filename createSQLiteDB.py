import glob
from tqdm import tqdm
from subprocess import PIPE, Popen
import pandas as pd
from joblib import Parallel, delayed
import multiprocessing
import os
import yaml
import sys
import pandas as pd
import sqlite3


gnomad_vcf_location = sys.argv[1] 
tables_out_location = "."
genome = "Grch38_gnomADv3.1.2"

with open("gnomad_columns.yaml") as f:
    columns = yaml.load(f, Loader=yaml.FullLoader)
columns = columns["base_columns"] + columns[genome]
print(columns)

# get gnomAD files
files = glob.glob(f"{gnomad_vcf_location}/*.bgz")
print(files)

# write gnomAD files to these tables:
tables_location = [f'{tables_out_location}/{file.split("/")[-1].replace(".vcf.bgz", "")}.tsv.gz' for file in files]
print(tables_location)

cpu_count = int(multiprocessing.cpu_count())

# extract needed columns
def create_table(file, table_location):
    query_string = "%" + "\t%".join(columns) + "\n"
    if not os.path.exists(table_location):
        cmd = f"bcftools query -f '{query_string}' {file} | gzip > {table_location}"
        p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        print(p.communicate())
        
# run bcftools in parallel
Parallel(cpu_count)(delayed(create_table)(file, table_location) for file, table_location in tqdm(zip(files, tables_location)))

tables_tsv = glob.glob(f"{tables_out_location}/*.tsv.gz")
print(tables_tsv)

data = "gnomAD3_nfe/data"
if not os.path.exists(data):
    os.mkdir(data)

conn = sqlite3.connect("gnomAD3_nfe/data/gnomAD3_nfe.sqlite")
curs = conn.cursor()
for table in tables_tsv:
    df = pd.read_csv(table, header = None, sep = '\t')
    df.columns = columns
    chrom = df['CHROM'].unique()[0]    
    df['nheter'] = df['AC'] - 2 * df['nhomalt']
    df['nheter_nfe'] = df['AC_nfe'] - 2 * df['nhomalt_nfe']
    
    if chrom == 'chrY':
        df['male_female_ratio'] = 0
        df['male_female_ratio_nfe'] = 0
    else:
        df.loc[(df['AC_XY'] > 0) & (df['AC_XX'] > 0), 'male_female_ratio'] = ((df['AC_XY'] - 2 * df['nhomalt_XY']) 
        + df['nhomalt_XY']) / ((df['AC_XX'] - 2 * df['nhomalt_XX']) + df['nhomalt_XX'])
        df['male_female_ratio'].fillna(0, inplace = True)
        df.loc[(df['AC_nfe_XY'] > 0) & (df['AC_nfe_XX'] > 0), 'male_female_ratio_nfe'] = ((df['AC_nfe_XY'] - 
        2 * df['nhomalt_nfe_XY']) + df['nhomalt_nfe_XY']) / ((df['AC_nfe_XX'] - 2 * df['nhomalt_nfe_XX']) + df['nhomalt_nfe_XX'])
        df['male_female_ratio_nfe'].fillna(0, inplace = True)


    chrom = df['CHROM'].unique()[0]
    curs.execute('''CREATE TABLE {chrom} (pos integer not null, 
                ref text not null, alt text not null, 
                ac integer not null, af real not null,
                nhomalt integer not null, nhomalt_xy int,
                nhomalt_xx int, ac_xy int, ac_xx int, ac_nfe int not null, 
                af_nfe real not_null, nhomalt_nfe int not null,
                ac_nfe_xy int, ac_nfe_xx int, nhomalt_nfe_xy int, nhomalt_nfe_xx int,
                nheter int not null, nheter_nfe int not null,
                male_female_ratio real not null, male_female_ratio_nfe real not null)'''.format(chrom = chrom))
    curs.execute('''CREATE INDEX {chrom}_ind on {chrom} (pos)'''.format(chrom=chrom))
    df.iloc[0:, 1:].to_sql(chrom, conn, if_exists='append', index = False)

conn.close()
