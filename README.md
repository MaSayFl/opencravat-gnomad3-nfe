# gnomAD3v3.1 frequency and counts nfe

The Genome Aggregation Database (gnomAD) is a resource developed by an international coalition of investigators, with the goal of aggregating and harmonizing both exome andgenome sequencing data from a wide variety of large-scale sequencing projects, and making summary data available for the wider scientific community.

## 1. SQLite database creation

You need preinstalled ```bcftools``` to work with vcf files.

**createSQLiteDB.py** creates SQLite database from gnomAD vcf files in nfe_gnomad3/data directory. Through this step script creates preprocessed intermediate tsv files and then uses them for SQLite. ```gnomad_columns.yaml``` contains fields that will be extracted from *vcf* files. 

```bash
python3 createSQLiteDB.py <path-to-vcf-files>
```
After check ```gnomAD3_nfe/data``` folder and there should be ```gnomAD3_nde.sqlite``` file. 

## 2. Writing an annotator

OpenCRAVAT should be already preinstalled. If not, please visit [this page](https://open-cravat.readthedocs.io/en/latest/1.-Installation-Instructions.html)

To begin writing a new annotator, first locate the path to the modules directory
using the command ```oc config md```.
```bash
$ oc config md
```

The modules directory contains all OpenCRAVAT modules, split into sub-directories by type. Annotator modules will be found in the sub-directory ```annotators```. This is where a developer will create their own annotator.

To start, use ```oc``` to create a new gnomAD3_nfe annotator.
```bash
$ oc new annotator gnomAD3_nfe
annotator gnomAD3_nfe created at cravat/modules/annotators/gnomAD3_nfe
```

This will create a few files at ```cravat/modules/annotators/gnomAD3_nfe```

After you need to copy all files from ```opencravat-gnomad3-nfe/gnomAD3_nfe``` to ```cravat/modules/annotators/gnomAD3_nfe```

```bash
cp -r <path-to-opencravat-gnomad3-nfe>/gnomAD3_nfe/* <path-to-cravat-modules>/annotators/gnomAD3_nfe
```

## 3. Run gnomAd3_nfe annotator and check results

Run annoatator with ```oc run <path-to-input.vcf> -a gnomAD3_nfe -l hg38``` and check out the output with ```oc gui <path-to-input.vcf.sqlite>```. 









