import sys
from cravat import BaseAnnotator
from cravat import InvalidData
import sqlite3
import os

class CravatAnnotator(BaseAnnotator):

    def setup(self): 
        pass
    
    def annotate(self, input_data):
        col_names = ["ac","af","nhomalt", "nheter", "male_female_ratio", "ac_nfe","af_nfe","nhomalt_nfe", "nheter_nfe", "male_female_ratio_nfe"]
        out = {x:'' for x in col_names}
        chrom = input_data['chrom']
        pos = input_data['pos']
        ref = input_data['ref_base']
        alt = input_data['alt_base']
        q = 'select %s from %s where pos=%s and ref="%s" and alt="%s";' \
                %(', '.join(col_names), chrom, pos, ref, alt)
        self.cursor.execute(q)
        qr = self.cursor.fetchone()
        if qr:
            for i, k in enumerate(col_names):
                out[k] = qr[i]
        return out
    
    def cleanup(self):
        pass
        
if __name__ == '__main__':
    annotator = CravatAnnotator(sys.argv)
    annotator.run()
